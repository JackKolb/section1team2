# **Overall plan for development**

**MW 3 Group:**
	Hanna Jones
	Ian Kaufman
	Jim Kim
	Adam Lee
	Simeon Martinez
	Trever Mock
	Ryan Newsom



**Communication**:
1. All groups have been assigned an ambassador from the MW 3 group.
2. Reach out to the ambassador if there are any questions or concerns during any of the phases of development.
3. Be advised that question and concerns should be addressed to the ambassador as soon as possible so they can address them immediately. 



**Documents**:
1. All factory reports will need to be submitted as a .txt file (this may change in the future).
2. PDF files will also be accepted for other documents.



**Naming conventions:**
1. Naming conventions will need to be consistent during the coding process and all groups will need to follow these conventions.
2. The document for naming conventions will be available on the dev branch.



**Pull requests:**
1. Pull requests consist of documents and code changes.
2. All pull requests will need to be submitted to the ambassador of the MW 3 group.


	
**Merge conflicts, build errors from a recently pulled repo:**
1. Always build and run the most recent repo. If any issues occur such as build errors, please contact your ambassador so they can let the people in charge of the repo know and they will see if there was a merge conflict or some other issue for a pull request that was done before. 
2. The people in charge of the repo will try to find where the issue occurred and rollback if necessary in order to get things back on track.


			

# **What will groups take on at each step:**


**Before any coding can begin:**
1. Factory report document must be done and approved before the next step in the redesign can occur.
2. All factory reports will need to be peer reviewed within the group by each member.
3. After the factory reports have been peer reviewed within the group, it will then need to be passed onto another group for a peer review process and approved from that group. As a final quality assurance check, the ambassador for each group will then go over the factory report.
4. Address any and all concerns related to the factory that may occur during the coding process such as assumptions, dependencies, or merge conflicts.
5. Only at this point, the document can be submitted to Dr. Dollard and Dr. Beaty for final submission and/or approval to move onto the next phase.



**During the coding process:**
1. Since there needs to be 90% unit test coverage on all the code written, all groups will need to get familiar with unit testing and which tools will be used if any to provide the unit tests.
2. Each group will need to have a member from their group to communicate with the ambassador from the MW 3 group to coordinate when merges or concerns come into question so they can be addressed immediately. The communication can be done via Slack or have the group member be added to a particular Slack channel.
6. Documentation must be done for all changes.
4. Merges will only occur on the days assigned (TBD) to each group unless otherwise approved by Dr. Dollard or Dr. Beaty.
5. Address any issues that may have come up during the process and areas that may have been unclear and any assumptions made.
6. A peer review process of the completed task will need to occur by another group to make sure there are no bugs or issues before any merge can occur. As a final quality assurance check, the ambassador for each group will then go over the code.
7. Once both groups and the ambassador have concluded that the working code is complete, the merge can be done.
8. If issues occur, then the group will not be allowed to merge until the issues have been resolved.
9. Merge conflicts will occur and they will need to be addressed accordingly.



**Submissions**:
1. All submissions should occur on the day Dr. Beaty has assigned on Moodle for the assignment submission.
2. All bugs and uncompleted tasks will need to be clearly stated and an explanation of why these bugs or uncompleted tasks needs to be documented.
