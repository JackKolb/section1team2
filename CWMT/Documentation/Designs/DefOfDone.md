# Definition of Done  
When is implementation done.

## Pre-Implementation  
####Requirements for a developer to begin development:   
* User Story tracked in some project management system (Jira, Trello, Rally)  
* Dependencies completed (Arch & Design docs in repo)  
* Acceptance Criteria defined  

## Acceptance Criteria  
* Criteria that has been defined for a User Story to be considered "completed"  
* At least one requirement has been created  
* Owned by whoever owns the story  

## Testing & Documentation  
* 90% test coverage  
* Coding conventions guidelines & documentation guidelines followed  

## To be merged  
* Pull request created & aprroved by group ambassador  
* Building succesfully  

## Done
* Code is merged into dev  
* Application is compiling and building       
* Features working w/ no major defects    
