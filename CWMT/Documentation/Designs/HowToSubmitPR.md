###Submitting a Pull Request  

##Steps   
	*Fork the CWMasterTeacher Repository - Bitbucket     
	*Clone your forked version of the repository - Your machine    
	*Switch to dev branch  - Your machine     
	*Add your changes - Your Machine   
	*Commit your changes - Your Machine
	*Push your changes to your local repo - Your machine      
	*Submit a Pull Request to CWMasterTeacher dev branch - BitBucket     
	*Ping @adam or @rnewsom - Slack   