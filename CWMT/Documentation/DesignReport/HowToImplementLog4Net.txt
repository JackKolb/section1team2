Error handling and logging with Log4Net - 

David Kirch, Brendan Richards, Wes Rollings, Erik Sandstrom, Sean Vincent, Jude Wren

How to implement Log4Net into “CWMasterTeacher3”
The following describe how to implement Log4Net into CW Master Teacher 3 - 

Adding Log4Net to Visual Studio - 
    Right click on References in the Solution Explorer window
    Then “Manage NuGet Packages”
    Search for “Log4Net”
    The first one should be the official NuGet package and install it
    Now, you should see the “Log4Net” dll under references

Specific code changes - include code examples, what about web logger?

In the Weblogger.cs file - 
    On line 10 it should read - public class WebLogger
    Starting on line 12, replace lines 12 - 16 with the following code - 
 
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger("WebLogger.cs");
        protected static WebLogger _log;
        private WebLogger()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
        public  static WebLogger Log
        {
            get
            {
                if (_log == null)
                {
                    _log = new WebLogger();   
                }
                return _log;
            }
        }
        protected void Write(string message)
        {
            logger.Debug(message);
        }


(Before edits numbering) on line 31, replace the code in the write method with the following statement:
logger.Debug(message);

Line 21 should still read - public static WebLogger Log

In the Web.config file - 
    Starting on line 8 and removing code from line 8-16, after <configSections>, enter the following lines of code -

    <section name="log4net" type ="log4net.Config.Log4NetConfigurationSectionHandler, log4net"/>
      
    <section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
    <!-- For more information on Entity Framework configuration, visit http://go.microsoft.com/fwlink/?LinkID=237468 -->
    <sectionGroup name="dotNetOpenAuth" type="DotNetOpenAuth.Configuration.DotNetOpenAuthSection, DotNetOpenAuth.Core">
      <section name="messaging" type="DotNetOpenAuth.Configuration.MessagingElement, DotNetOpenAuth.Core" requirePermission="false" allowLocation="true" />
      <section name="reporting" type="DotNetOpenAuth.Configuration.ReportingElement, DotNetOpenAuth.Core" requirePermission="false" allowLocation="true" />
      <section name="openid" type="DotNetOpenAuth.Configuration.OpenIdElement, DotNetOpenAuth.OpenId" requirePermission="false" allowLocation="true" />
      <section name="oauth" type="DotNetOpenAuth.Configuration.OAuthElement, DotNetOpenAuth.OAuth" requirePermission="false" allowLocation="true" />
    </sectionGroup>
  </configSections>

  <log4net>  
     <appender name="TraceAppender" type="log4net.Appender.TraceAppender">
       <layout type="log4net.Layout.PatternLayout">
         <conversionPattern value="%date{ABSOLUTE} [%thread] %level - %message%newline%exception" />
       </layout>
     </appender>
    
    <appender name="FileAppender" type="log4net.Appender.FileAppender">
       <file value="C:\Users\Quend0\Desktop\mylogfile.txt" />
        <appendToFile value="true" />
      <lockingModel type="log4net.Appender.FileAppender+MinimalLock" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date{ABSOLUTE} [%thread] %level - %message%newline%exception" />
      </layout>
    </appender>
    
    <root>
      <level value="DEBUG" />
      <appender-ref ref="FileAppender" />
    </root>
  </log4net>

This should be followed be a line containing “<connectionStrings>”

To implement a different logger instead of Log4Net -
    Inputting a different logging system should only require removing the specific code here and replacing it with code for whatever logging system you choose.  Any other logger will likely have a similar tutorial or guide as Log4Net did, which was fairly easy to follow.

To change the appender -
    The resources below have very specific and easy to follow instructions to change the appenders.  We chose this one based on our specifications for the assignment,, but in the future it would be a relatively simple task to change the appender.

Resources - 
    Video showing how to implement Log4Net in C#-
        https://www.youtube.com/watch?v=2lAdQ_QwNww
    Git Repository for the above video
        https://github.com/TimCorey/Log4netTutorial
    Article that preceded the video above
        http://www.codeproject.com/Articles/140911/log-net-Tutorial
    Log4Net website
        http://logging.apache.org/log4net/