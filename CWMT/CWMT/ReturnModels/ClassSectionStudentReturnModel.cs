﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CWMT.ReturnModels
{
    public class ClassSectionStudentReturnModel
    {
        public bool HasBeenApproved { get; set; }
        public bool HasBeenDenied { get; set; }
    }
}