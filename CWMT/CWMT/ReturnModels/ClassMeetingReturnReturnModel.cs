﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CWMasterTeacher3.ReturnModels
{
    public class ClassMeetingReturnReturnModel
    {
        public bool IsReadyToTeach { get; set; }
        public Guid ClassMeetingId { get; set; }

    }
}