﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWMasterTeacherDomain.DomainObjects
{
    /// <summary>
    /// ClassMeetingDomainObjBasic abstracts the dependencies of the db from the rest of the application
    /// by taking the basic information from the Class DbObject and storing it in the BasicDomain object
    /// </summary>
    public class ClassMeetingDomainObjBasic
    {
        /// <summary>
        /// Unique identifier of this ClassMeetingDomainObjBasic.
        /// Used as the identifier of the ClassMeetingDomainObj this basic object
        /// belongs to.
        /// </summary>
        /// 
		public Guid Id { get; set; }

        /// <summary>
        /// Here in Basic because used in Classroom View 
        /// </summary>
        public string ClassSectionName { get; set; }

        /// <summary>
        /// Used in Classroom View
        /// </summary>
        public Guid ClassSectionId { get; set; }

        /// <summary>
        /// Here in Basic in order to generate the Date String.
        /// </summary>
        public DateTime MeetingDate { get; set; }

        /// <summary>
        /// String for the date, used in Classroom View
        /// </summary>
        public string MeetingDateString
        {
            get { return DomainWebUtilities.DateTime_ToLongDateString(MeetingDate); }
        }


        /// <summary>
        /// Not set from Db, used in Classroom View and set in ClassroomService.
        /// This value exists independently in both DomainObj and Basic but those are independent
        /// and used in DailyPlanning and Classroom respectively
        /// </summary>
        public bool IsSelected { get; set; }

        /// Is this class meeting ready to be taught?
        /// This value has to exist independently in both DomainObj and Basic in order to 
        /// function correctly in both DailyPlanningView and ClassroomView
        public bool IsReadyToTeach { get; set; }

        public string DisplayClass
        {
            get
            {
                return DomainWebUtilities.ListSelectedClass(IsSelected, true) + " " + DomainWebUtilities.ListCurrentClass(IsCurrent);
            }
        }

        public bool IsCurrent { get; set; }



    }//End Class   
}
