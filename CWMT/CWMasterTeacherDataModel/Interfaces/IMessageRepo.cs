﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWMasterTeacherDataModel.Interfaces
{
	public interface IMessageRepo : IRepository<Message>
	{
		IEnumerable<Message> MessagesForThreadParent(Guid threadParentId);

	}
}
