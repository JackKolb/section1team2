﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CWMasterTeacherDomain;
using CWMasterTeacherDomain.DomainObjects;

namespace CWMasterTeacherDataModel.Interfaces
{
    public interface ILessonForComparisonDomainObjBuilder : Interfaces.IDomainObjBuilder<LessonForComparisonDomainObj, LessonDomainObjBasic, Lesson>
    {
        
                
    }
}
