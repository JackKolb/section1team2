﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWMasterTeacherDataModel.Interfaces
{
	public interface IUserRepo : IRepository<User>
	{
		IEnumerable<User> ApprovedUsersForWorkingGroup(Guid workingGroupId);
		IEnumerable<User> AllUsersForWorkingGroup(Guid workingGroupId);

		User UserByUserName(string userName);
	}
}