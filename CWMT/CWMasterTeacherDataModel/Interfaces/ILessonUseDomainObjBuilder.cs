﻿using CWMasterTeacherDomain.DomainObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWMasterTeacherDataModel.Interfaces
{
    public interface ILessonUseDomainObjBuilder : Interfaces.IDomainObjBuilder<LessonUseDomainObj, LessonUseDomainObjBasic, LessonUse>
    {
        List<LessonUseDomainObj> LessonUseObjsForClassMeeting(Guid classMeetingId);
        string GetTextForLessonUse(Guid lessonUseId);
    }
}
