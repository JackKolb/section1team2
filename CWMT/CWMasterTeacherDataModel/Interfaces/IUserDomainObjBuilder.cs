using CWMasterTeacherDomain.DomainObjects;
using System.Collections.Generic;
using System;

namespace CWMasterTeacherDataModel.Interfaces
{
    /// <summary>
    /// Interface to be implemented by classes wishing to provide services that build and return UserDomainObj.
    /// </summary>
    public interface IUserDomainObjBuilder : IDomainObjBuilder<UserDomainObj, UserDomainObjBasic, User>
    {
        List<UserDomainObjBasic> GetAllUsersForWorkingGroupList(Guid workingGroupId);
        UserDomainObj BuildFromUserName(string userName);
        void SetLastDisplayedCourseId(Guid userId, Guid courseId);
    }
}
