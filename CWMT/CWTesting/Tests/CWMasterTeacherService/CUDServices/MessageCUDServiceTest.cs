﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using CWMasterTeacherService.CUDServices;
using CWMasterTeacherDataModel;
using CWMasterTeacherDataModel.ObjectBuilders;
using System.Data.Entity;
using CWMasterTeacherDataModel.Interfaces;




namespace CWTesting.Tests.CWMasterTeacherService.CUDServices
{
	class MessageCUDServiceTest
	{
		private Mock<IMessageRepo> mockMessageRepo; 
		private Mock<ILessonRepo> mockLessonRepo;
		private Mock<IMessageUseRepo> mockMessageUseRepo; 
		private Mock<ICourseRepo> mockCourseRepo;
		private Mock<INarrativeRepo> mockNarrativeRepo;
		private Mock<IDocumentUseRepo> mockDocumentUseRepo;
		private Mock<ILessonPlanRepo> mockLessonPlanRepo;
		private Mock<ITermRepo> mockTermRepo;
		private Mock<IUserRepo> mockUserRepo;
		private Mock<IWorkingGroupRepo> mockWorkingGroupRepo;
		private Mock<IMetaCourseRepo> mockMetaCourseRepo;
		private Mock<IMetaLessonRepo> mockMetaLessonRepo;
		private Mock<IStashedLessonPlanRepo> mockStashedLessonPlanRepo;
		private Mock<IStashedNarrativeRepo> mockStashedNarrativeRepo;
		private Mock<IUserPreferenceRepo> mockUserPreferenceRepo;
		private Mock<ICoursePreferenceRepo> mockCoursePreferenceRepo;
		private Mock<IMessageUseDomainObjBuilder> mockMessageUseDomainObjBuilder;
		private Mock<ILessonDomainObjBuilder> mockLessonDomainObjBuilder;
	
		private MessageUse _messageUse;
		private MessageUse _messageUse2;
		private MessageUse _messageUse3;

		private List<MessageUse> data;
		private List<Message> dat;

		private Message _message;
		private Guid testGuid = Guid.NewGuid();
		private Guid guidTest = Guid.NewGuid();
		private Guid guidId = Guid.NewGuid();
		private Guid useId = Guid.NewGuid();
		private Guid prevId = Guid.NewGuid();
		private string testString = "Function Of HeadLess Duck";
		private string stringTest = "Not that other string";
		private bool testBool = true;
		private bool boolTest = false;
		private DateTime testDate = new DateTime(2018, 04, 20);

		DocumentUseCUDService documentUseCUD;
		StashedLessonPlanCUDService stashedLessonPlanCUD;
		MessageUseCUDService messageUseCUD;
		LessonPlanCUDService lessonPlanCUD;
		StashedNarrativeCUDService stashedNarrativeCUD;
		SharedCUDService sharedCUD;
		NarrativeCUDService narrativeCUD;
		UserCUDService userCUD;
		LessonCUDService lessonCUD;
		CoursePreferenceCUDService coursePrefCUD;
		CourseCUDService courseCUD;

		Exception e;

		[OneTimeSetUp]
		public void setUp()
		{	//Repos and Builders
			mockMetaCourseRepo = new Mock<IMetaCourseRepo>();
			mockNarrativeRepo = new Mock<INarrativeRepo>();
			mockDocumentUseRepo = new Mock<IDocumentUseRepo>();
			mockLessonPlanRepo = new Mock<ILessonPlanRepo>();
			mockMessageUseRepo = new Mock<IMessageUseRepo>();
			mockTermRepo = new Mock<ITermRepo>();
			mockLessonRepo = new Mock<ILessonRepo>();
			mockWorkingGroupRepo = new Mock<IWorkingGroupRepo>();
			mockUserRepo = new Mock<IUserRepo>();
			mockStashedLessonPlanRepo = new Mock<IStashedLessonPlanRepo>();
			mockStashedNarrativeRepo = new Mock<IStashedNarrativeRepo>();
			mockCourseRepo = new Mock<ICourseRepo>();
			mockMessageRepo = new Mock<IMessageRepo>();
			mockMetaLessonRepo = new Mock<IMetaLessonRepo>();
			mockUserPreferenceRepo = new Mock<IUserPreferenceRepo>();
			mockCoursePreferenceRepo = new Mock<ICoursePreferenceRepo>();
			mockMessageUseDomainObjBuilder = new Mock<IMessageUseDomainObjBuilder>();
			mockMessageUseDomainObjBuilder.SetupAllProperties();
			mockLessonDomainObjBuilder = new Mock<ILessonDomainObjBuilder>();
			mockLessonDomainObjBuilder.SetupAllProperties();

			//CUD Objects
			documentUseCUD = new DocumentUseCUDService(mockDocumentUseRepo.Object, mockLessonRepo.Object);
			stashedLessonPlanCUD = new StashedLessonPlanCUDService(mockStashedLessonPlanRepo.Object, mockLessonRepo.Object, mockLessonPlanRepo.Object);
			messageUseCUD = new MessageUseCUDService(mockMessageUseRepo.Object);
			lessonPlanCUD = new LessonPlanCUDService(mockLessonPlanRepo.Object, stashedLessonPlanCUD, mockLessonRepo.Object);
			stashedNarrativeCUD = new StashedNarrativeCUDService(mockStashedNarrativeRepo.Object, mockLessonRepo.Object);
			sharedCUD = new SharedCUDService(mockLessonRepo.Object, mockCourseRepo.Object, mockMessageUseRepo.Object);
			narrativeCUD = new NarrativeCUDService(mockNarrativeRepo.Object, mockLessonRepo.Object, mockStashedNarrativeRepo.Object,
						mockCourseRepo.Object, stashedNarrativeCUD, sharedCUD);
			userCUD = new UserCUDService(mockUserRepo.Object, mockWorkingGroupRepo.Object, mockUserPreferenceRepo.Object);
			lessonCUD = new LessonCUDService(null, mockLessonRepo.Object, mockNarrativeRepo.Object,
					mockDocumentUseRepo.Object, mockLessonPlanRepo.Object, mockStashedLessonPlanRepo.Object, mockMessageUseRepo.Object,
					mockTermRepo.Object, mockMetaLessonRepo.Object, mockCourseRepo.Object, documentUseCUD, mockStashedNarrativeRepo.Object,
					stashedLessonPlanCUD, messageUseCUD, lessonPlanCUD, narrativeCUD, mockMessageUseDomainObjBuilder.Object, sharedCUD,
					mockLessonDomainObjBuilder.Object);
			coursePrefCUD = new CoursePreferenceCUDService(mockCoursePreferenceRepo.Object);
			courseCUD = new CourseCUDService(mockCourseRepo.Object, mockLessonRepo.Object, mockUserRepo.Object,
					mockMetaCourseRepo.Object, lessonCUD, userCUD, mockCoursePreferenceRepo.Object, coursePrefCUD, sharedCUD);

			
			//MessageUse Object setup
			_messageUse = new MessageUse();
			_messageUse.Lesson = new Lesson();
			_messageUse.Lesson.Course = new Course();
			_messageUse.Lesson.Course.Term = new Term();
			_messageUse.Message = new Message();
			_messageUse.MessageId = testGuid;
			_messageUse.Lesson.Course.Term.StartDate = testDate;
			_messageUse.Id = testGuid;
			_messageUse.IsStored = testBool;
			_messageUse.IsImportant = true;
			_messageUse.IsNew = true;

			_messageUse2 = new MessageUse();
			_messageUse2.Message = new Message();
			_messageUse2.MessageId = testGuid;
			
			_messageUse2.Message.Text = testString;

			_messageUse3 = new MessageUse();
			_messageUse3.Message = new Message();
			_messageUse3.MessageId = guidTest;
			_messageUse3.Message.Text = stringTest;
			_messageUse3.LessonId = testGuid;

			//MessageRepo setup
			dat = new List<Message>();

			mockMessageRepo.Setup(mock => mock.Insert(It.IsAny<Message>())).Callback((Message i) =>
			{
				if (i.Id == Guid.Empty)
				{

					i.Id = Guid.NewGuid();
				}

				dat.Add(i);
				guidId = i.Id;
			});
			mockMessageRepo.Setup(mock => mock.GetById(It.IsAny<Guid>())).Returns((Guid i) =>
            {
                return dat.Find(x => x.Id == i);
            });

			//MessageUseRepo setup
			data = new List<MessageUse>() {
				_messageUse,
				
			};

			mockMessageUseRepo.Setup(mock => mock.GetMessageUsesForMessage(It.IsAny<Guid>())).Returns((Guid i) =>
			{
				return data.Where(x => x.MessageId == i).ToList();

			});

			mockMessageUseRepo.Setup(mock => mock.Insert(It.IsAny<MessageUse>())).Callback((MessageUse i) =>
			{
				if (i.Id == Guid.Empty)
				{

					i.Id = Guid.NewGuid();
				}

				data.Add(i);
				useId = i.Id;
			});
			 
			mockMessageUseRepo.Setup(mock => mock.GetById(It.IsAny<Guid>())).Returns((Guid i) =>
			{
				return data.Find(x => x.Id == i);
			});

			mockMessageUseRepo.Setup(mock => mock.Update(It.IsAny<MessageUse>())).Callback((MessageUse i) =>
			{
				if (!data.Remove(data.Find(x => x.Id == i.Id))) throw e;
				data.Add(i);
			});

		}

		[Test]
		public void CreateTest()
		{

			MessageCUDService obj = new MessageCUDService(mockMessageRepo.Object, mockLessonRepo.Object, mockMessageUseRepo.Object,
						mockCourseRepo.Object, courseCUD, lessonCUD, messageUseCUD, sharedCUD);

			obj.Create(testGuid, guidTest, testString, stringTest);
			Assert.AreEqual(testGuid, mockMessageRepo.Object.GetById(guidId).AuthorId);
			Assert.AreEqual(guidTest, mockMessageRepo.Object.GetById(guidId).ThreadParentId);
			Assert.AreEqual(testString, mockMessageRepo.Object.GetById(guidId).Subject);
			Assert.AreEqual(stringTest, mockMessageRepo.Object.GetById(guidId).Text);
		}

		[Test]
		public void PostMessageTest()
		{
			
			MessageCUDService obj = new MessageCUDService(mockMessageRepo.Object, mockLessonRepo.Object, mockMessageUseRepo.Object,
							mockCourseRepo.Object, courseCUD, lessonCUD, messageUseCUD, sharedCUD);

			obj.PostMessage(testGuid, Guid.Empty, Guid.Empty, testString, testString, boolTest, boolTest);
			//parentMessageId is empty
			Assert.AreEqual(testString, mockMessageRepo.Object.GetById(guidId).Subject);
			Assert.AreEqual(Guid.Empty, mockMessageRepo.Object.GetById(guidId).AuthorId);
			Assert.AreEqual(testString, mockMessageRepo.Object.GetById(guidId).Text);
			Assert.AreEqual(null, mockMessageRepo.Object.GetById(guidId).ThreadParentId);
			//parentMessageId not empty
			obj.PostMessage(testGuid, guidTest, Guid.Empty, testString, stringTest, boolTest, boolTest);
			Assert.AreEqual(testString, mockMessageRepo.Object.GetById(guidId).Subject);
			Assert.AreEqual(Guid.Empty, mockMessageRepo.Object.GetById(guidId).AuthorId);
			Assert.AreEqual(stringTest, mockMessageRepo.Object.GetById(guidId).Text);
			Assert.AreEqual(guidTest, mockMessageRepo.Object.GetById(guidId).ThreadParentId);
		}
		[Test]
		public void PostMessageMessageUseListTest()
		{

			mockMessageUseRepo.Object.Insert(_messageUse);
			mockMessageUseRepo.Object.Insert(_messageUse2);
			mockMessageUseRepo.Object.Insert(_messageUse3);
			MessageCUDService obj = new MessageCUDService(mockMessageRepo.Object, mockLessonRepo.Object, mockMessageUseRepo.Object,
							mockCourseRepo.Object, courseCUD, lessonCUD, messageUseCUD, sharedCUD);
			obj.PostMessage(testGuid, guidTest, Guid.Empty, testString, stringTest, boolTest, boolTest);

			Assert.AreEqual(mockMessageRepo.Object.GetById(guidId).Id, mockMessageUseRepo.Object.GetById(useId).MessageId);
			Assert.AreEqual(_messageUse3.LessonId, mockMessageUseRepo.Object.GetById(useId).LessonId);
		}

		[Test]
		public void UpdateIsStoredTest()
		{

			MessageCUDService obj = new MessageCUDService(mockMessageRepo.Object, mockLessonRepo.Object, mockMessageUseRepo.Object,
						mockCourseRepo.Object, courseCUD, lessonCUD, messageUseCUD, sharedCUD);
			obj.UpdateIsStored(testGuid, testBool);
			Assert.AreEqual(_messageUse.StorageReferenceTime.Value.Hour, mockMessageUseRepo.Object.GetById(testGuid).StorageReferenceTime.Value.Hour);
		}

		[Test]
		public void UpdateIsNewTest()
		{
			MessageCUDService obj = new MessageCUDService(mockMessageRepo.Object, mockLessonRepo.Object, mockMessageUseRepo.Object,
						mockCourseRepo.Object, courseCUD, lessonCUD, messageUseCUD, sharedCUD);
			obj.UpdateIsNew(testGuid, boolTest);
			Assert.IsFalse(_messageUse.IsNew);
			Assert.AreEqual(boolTest, mockMessageUseRepo.Object.GetById(testGuid).IsNew);
		}

		[Test]
		public void UpdateIsImportantTest()
		{
			MessageCUDService obj = new MessageCUDService(mockMessageRepo.Object, mockLessonRepo.Object, mockMessageUseRepo.Object,
						mockCourseRepo.Object, courseCUD, lessonCUD, messageUseCUD, sharedCUD);
			obj.UpdateIsImportant(testGuid, boolTest);
			Assert.IsFalse(_messageUse.IsImportant);
			Assert.AreEqual(boolTest, mockMessageUseRepo.Object.GetById(testGuid).IsImportant);
		}

		[Test]
		public void UpdateIsArchivedTest()
		{
			MessageCUDService obj = new MessageCUDService(mockMessageRepo.Object, mockLessonRepo.Object, mockMessageUseRepo.Object,
						mockCourseRepo.Object, courseCUD, lessonCUD, messageUseCUD, sharedCUD);
			obj.UpdateIsArchived(testGuid, testBool);
			Assert.IsTrue(_messageUse.IsArchived);
			Assert.AreEqual(testBool, mockMessageUseRepo.Object.GetById(testGuid).IsArchived);
		}

		[Test]
		public void UpDateIsForEndOfSemRevTest()
		{
			MessageCUDService obj = new MessageCUDService(mockMessageRepo.Object, mockLessonRepo.Object, mockMessageUseRepo.Object,
						mockCourseRepo.Object, courseCUD, lessonCUD, messageUseCUD, sharedCUD);
			obj.UpdateIsForEndOfSemRev(testGuid, boolTest);
			Assert.IsFalse(_messageUse.IsForEndOfSemRev);
			Assert.AreEqual(boolTest, mockMessageUseRepo.Object.GetById(testGuid).IsForEndOfSemRev);
		}

	}
}
