﻿using CWMasterTeacherDataModel;
using CWMasterTeacherDataModel.ObjectBuilders;
using CWMasterTeacherService.CUDRouters;
using CWMasterTeacherService.CUDServices;
using CWTesting.Tests.NEWMocks;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CWTesting.Tests.NEWMocks.Mocks;

namespace CWTesting.Tests.CWMasterTeacherService
{
	[TestFixture]
	class MessageBoardTabCUDRouterTests
	{
		MessageBoardTabCUDRouter router;

		[OneTimeSetUp]
		public void setup()
		{
			router = new MessageBoardTabCUDRouter(CUDServices_Message, DomainBuilders_MessageUse, CUDServices_Shared);
			Objects_MessageUse.IsArchived = true;
			Objects_MessageUse.IsForEndOfSemRev = true;
			Objects_MessageUse.MessageId = DefaultId;
			Objects_MessageUse.Lesson = new Lesson();
			Objects_MessageUse.Lesson.Course = new Course();
			Objects_MessageUse.Lesson.Course.Term = new Term()
			{
				StartDate = DateTime.Now
			};
			Objects_MessageUse.Message = new Message();
			Objects_MessageUse.Message.User = new User();



		}
		[Test]
		public void TestPostMessage()
		{
			Objects_Lesson.Course = new Course();
			Objects_Lesson.Course.Term = new Term();
			Objects_Lesson.Course.Term.StartDate = DateTime.Now;
			router.PostMessage(DefaultId, DefaultId, DefaultId, "lol", null, true, true);
			
			Assert.AreEqual(Objects_Message.Text, Repos_Message.GetById(DefaultId).Text);
			Assert.AreEqual(Objects_Message.AuthorId, Repos_Message.GetById(DefaultId).AuthorId);
			
		}
	}

}
