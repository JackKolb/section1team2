﻿using CWMasterTeacherDataModel;
using CWMasterTeacherDataModel.ObjectBuilders;
using CWMasterTeacherDomain;
using CWMasterTeacherDomain.DomainObjects;
using CWMasterTeacherService.DomainObjectBuilders;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace CWTesting.Tests.CWMasterTeacherService.DomainObjectBuilders
{
    interface IMyCollection : ICollection<DocumentUse>
    {
        int Count { get; }
        DocumentUse this[int index] { get; }
    }
    [TestFixture]
    class LessonForComparisonDomainObjBuilderTests
    {
        private Mock<NarrativeRepo> mockNarrativeRepo;
        private Mock<DocumentUseRepo> mockDocUseRepo;
        private Mock<LessonPlanRepo> mockLessonPlanRepo;
        private Mock<MessageUseRepo> mockMessageUseRepo;
        private Mock<TermRepo> mockTermRepo;
        private Mock<LessonRepo> mockRepoReturnsNull;
        private Mock<LessonRepo> mockRepoReturnsLesson;
        private Mock<LessonDomainObjBasic> mockLessonDomainObjBasic;
        private Mock<LessonForComparisonDomainObj> mockLessonForComparisonDomainObj;
        private Mock<Lesson> mockLesson;
        private Guid mockGuid = Guid.Empty;
        private Guid validMockGuid = Guid.NewGuid();
        private Mock<Lesson> mockNullLesson;
        private Mock<LessonDomainObjBasic> mockBasic;
        private Mock<Course> mockCourse;
        private Mock<Narrative> mockNarrative;
        private Mock<LessonPlan> mockLessonPlan;
        private Mock<User> mockUser;
        private Mock<CoursePreference> mockCoursePreference;
        private Mock<IMyCollection> mockList;
        private Mock<Document> mockDoc;
        private Mock<DocumentUse> mockDocUse;
        private List<DocumentUse> realList;


        [SetUp]
        public void setup()
        {
            DateTime mockDate = new DateTime(20181217);
            DateTime? mockNullableDate = new DateTime(201812117);
            bool mockBool = true;
            string mockString = "Test";

            Mock<MasterTeacherContext> mockContext = new Mock<MasterTeacherContext>();

            mockNarrativeRepo = new Mock<NarrativeRepo>(mockContext.Object);
            mockDocUseRepo = new Mock<DocumentUseRepo>(mockContext.Object);
            mockLessonPlanRepo = new Mock<LessonPlanRepo>(mockContext.Object);
            mockMessageUseRepo = new Mock<MessageUseRepo>(mockContext.Object);
            mockTermRepo = new Mock<TermRepo>(mockContext.Object);


            mockRepoReturnsNull = new Mock<LessonRepo>(mockContext.Object, mockNarrativeRepo.Object, mockDocUseRepo.Object, mockLessonPlanRepo.Object, mockMessageUseRepo.Object, mockTermRepo.Object);

            mockRepoReturnsLesson = new Mock<LessonRepo>(mockContext.Object, mockNarrativeRepo.Object, mockDocUseRepo.Object, mockLessonPlanRepo.Object, mockMessageUseRepo.Object, mockTermRepo.Object);

            var basic = LessonDomainObjBuilder.BuildBasicEmpty();


            mockLessonForComparisonDomainObj = new Mock<LessonForComparisonDomainObj>(basic);
            mockLessonForComparisonDomainObj.SetupAllProperties();

            mockLessonForComparisonDomainObj.Object.ChangeNotes = "";
            mockLessonForComparisonDomainObj.Object.IsActive = false;
            mockLessonForComparisonDomainObj.Object.LessonPlanObj = LessonPlanDomainObjBuilder.BuildEmpty();
            mockLessonForComparisonDomainObj.Object.NarrativeObj = NarrativeDomainObjBuilder.BuildEmpty();
            mockLessonForComparisonDomainObj.Object.MasterLessonId = Guid.Empty;
            mockLessonForComparisonDomainObj.Object.DocumentsModifiedDate = new DateTime();
            mockLessonForComparisonDomainObj.Object.UserName = "_";


            mockList = new Mock<IMyCollection>();
            mockList.SetupAllProperties();


            mockDoc = new Mock<Document>();
            mockDoc.Object.DateModified = mockNullableDate;
            mockUser = new Mock<User>();
            mockUser.SetupAllProperties();
            mockUser.Object.DisplayName = mockString;

            mockUser.Object.FirstName = mockString;
            mockUser.Object.LastName = mockString;
            mockUser.Object.Id = mockGuid;
            mockDocUse = new Mock<DocumentUse>();
            mockDocUse.SetupAllProperties();
            mockDocUse.Object.IsReference = mockBool;
            mockDocUse.Object.IsActive = false;
            mockDocUse.Object.IsInstructorOnly = mockBool;
            mockDocUse.Object.IsOutForEdit = mockBool;
            mockDocUse.Object.IsVisibleToStudents = mockBool;
            mockDocUse.Object.LessonId = mockGuid;
            mockDocUse.Object.Document = mockDoc.Object;
            realList = new List<DocumentUse>();
            for (int i = 0; i < 10; i++)
            {
                realList.Add(mockDocUse.Object);
            }
            mockList.Setup(x => x.GetEnumerator()).Returns(() => realList.GetEnumerator());


            mockLesson = new Mock<Lesson>();
            mockLesson.SetupAllProperties();

            mockBasic = new Mock<LessonDomainObjBasic>(mockLesson);
            mockBasic.SetupAllProperties();


            mockCourse = new Mock<Course>();
            mockCourse.SetupAllProperties();
            mockCourse.Object.IsMaster = mockBool;
            mockLesson.Object.Name = "Test Name";
            mockLesson.Object.Course = mockCourse.Object;
            mockLesson.Object.Id = validMockGuid;
            mockLesson.Object.CourseId = validMockGuid;
            mockLesson.Object.NarrativeDateChoiceConfirmed = mockDate;
            mockLesson.Object.NarrativeReferenceDateChoiceConfirmed = mockDate;

            mockNarrative = new Mock<Narrative>();
            mockNarrative.SetupAllProperties();
            mockNarrative.Object.DateModified = mockDate;

            mockLessonPlan = new Mock<LessonPlan>();
            mockLessonPlan.SetupAllProperties();
            mockLessonPlan.Object.DateModified = mockDate;

            mockLesson.Object.Narrative = mockNarrative.Object;
            mockLesson.Object.LessonPlanDateChoiceConfirmed = mockDate;
            mockLesson.Object.LessonPlan = mockLessonPlan.Object;
            mockLesson.Object.DateTimeDocumentsChoiceConfirmed = mockDate;
            mockLesson.Object.IsHidden = mockBool;
            mockLesson.Object.IsCollapsed = mockBool;
            mockCourse.Object.User = mockUser.Object;
            mockLesson.Object.Course = mockCourse.Object;
            mockLesson.Object.DocumentUses = mockList.Object;
            mockLesson.Object.LessonPlanId = validMockGuid;
            mockLesson.Object.NarrativeId = validMockGuid;
            mockLesson.Object.Narrative.Text = "123";
            mockCoursePreference = new Mock<CoursePreference>();
            mockCoursePreference.SetupAllProperties();
            mockCoursePreference.Object.DoShowDocumentNotifications = mockBool;
            mockCoursePreference.Object.DoShowLessonPlanNotifications = mockBool;
            mockCoursePreference.Object.DoShowNarrativeNotifications = mockBool;


            mockCourse.Object.CoursePreference = mockCoursePreference.Object;

            mockLesson.Object.Course = mockCourse.Object;
            mockLesson.Object.Course.CoursePreference.DoShowLessonPlanNotifications = mockBool;
            mockLesson.Object.Course.CoursePreference.DoShowNarrativeNotifications = mockBool;


            mockDocUse.Object.Lesson = mockLesson.Object;
            mockRepoReturnsNull.Setup(p => p.GetById(mockGuid)).Returns<Lesson>(null);
            mockRepoReturnsLesson.Setup(p => p.GetById(validMockGuid)).Returns(mockLesson.Object);



        }
        [Test]
        public void TestBuildBasic()
        {
            LessonDomainObjBasic obj = LessonForComparisonDomainObjBuilder.BuildBasic(mockLesson.Object);
            LessonDomainObjBasic obj2 = LessonForComparisonDomainObjBuilder.BuildBasic(null);

            Assert.IsNotNull(obj);
            Assert.IsNull(obj2);


        }

        [Test]
        public void TestBuildFromID_EmptyGuid()
        {

            LessonForComparisonDomainObjBuilder obj = new LessonForComparisonDomainObjBuilder(mockRepoReturnsNull.Object, null, null, null, null);
            LessonForComparisonDomainObj obj2 = obj.BuildFromId(mockGuid);
            LessonForComparisonDomainObj emptyObj = obj.BuildEmpty();

            Compare(obj2, emptyObj);

        }

        [Test]
        public void TestBuildFromID_ValidGuid()
        {
           
            LessonForComparisonDomainObjBuilder obj = new LessonForComparisonDomainObjBuilder(mockRepoReturnsLesson.Object, mockDocUseRepo.Object, null, mockLessonPlanRepo.Object, null);
            LessonForComparisonDomainObj obj2 = obj.BuildFromId(validMockGuid);
            LessonForComparisonDomainObj builtObj = LessonForComparisonDomainObjBuilder.Build(mockLesson.Object);

         
            Compare(obj2, builtObj);

        }
        
        [Test]
        public void TestBuildBasicFromID_EmptyGuid()
        {
            LessonForComparisonDomainObjBuilder obj = new LessonForComparisonDomainObjBuilder(mockRepoReturnsNull.Object, null, null, null, null);
            LessonDomainObjBasic obj2 = obj.BuildBasicFromId(mockGuid);
            
            Assert.IsNull(obj2);
            
        }


        
        [Test]
        public void TestBuildBasicFromID_ValidGuid()
        {
            LessonForComparisonDomainObjBuilder obj = new LessonForComparisonDomainObjBuilder(mockRepoReturnsLesson.Object, mockDocUseRepo.Object, null, mockLessonPlanRepo.Object, null);
            LessonDomainObjBasic obj2 = obj.BuildBasicFromId(validMockGuid);
            LessonDomainObjBasic objBasic = LessonForComparisonDomainObjBuilder.BuildBasic(mockLesson.Object);

            Compare(obj2, objBasic);
            

        }

        [Test]
        public void TestBuildEmpty()
        {
            LessonForComparisonDomainObjBuilder obj = new LessonForComparisonDomainObjBuilder(mockRepoReturnsLesson.Object, mockDocUseRepo.Object, null, mockLessonPlanRepo.Object, null);
            LessonForComparisonDomainObj obj2 = obj.BuildEmpty();


            Compare(obj2, mockLessonForComparisonDomainObj.Object);
        }


        [Author("Dan Dews")]
        public void Compare<T>(T a, T b)
        {
            Console.WriteLine("Comparing: " + a + " and " + b);
            const BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance;
            List<MemberInfo> fields = new List<MemberInfo>();

            MemberInfo[] members = a.GetType().GetFields(bindingFlags);
            MemberInfo[] members2 = a.GetType().GetProperties(bindingFlags);
            
            for (int i = 0; i < members.Length; i++)
            {
                fields.Add(members[i]);
            }
            for (int i = 0; i < members2.Length; i++)
            {
                fields.Add(members2[i]);
            }

            for (int i = 0; i < fields.Count; i++)
            {
                
                MemberInfo info = fields[i];
                switch (info.MemberType)
                {
                    case MemberTypes.Property:
                        PropertyInfo prop = (PropertyInfo)info;
                        if (prop.PropertyType.IsGenericType || prop.PropertyType == typeof(Guid) || prop.PropertyType == typeof(string))
                        {
                            Assert.AreEqual(prop.GetValue(a), prop.GetValue(b));
                            Console.WriteLine(prop.Name + " is the same for both objects");
                        }
                            break;
                    case MemberTypes.Field:
                        FieldInfo field = (FieldInfo)info;

                        if (field.FieldType.IsGenericType || field.FieldType == typeof(Guid) || field.FieldType == typeof(string))
                        {
                            Assert.AreEqual(field.GetValue(a), field.GetValue(b));
                            Console.WriteLine(field.Name + " is the same for both objects");
                        }
                        break;

                }
            }

        }

    }
}