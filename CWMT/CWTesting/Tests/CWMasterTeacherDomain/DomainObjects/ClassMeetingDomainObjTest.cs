﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using NUnit.Framework;
using CWMasterTeacherDomain.DomainObjects;
using CWMasterTeacherDataModel;
using CWMasterTeacherDomain;

namespace CWTesting.Tests.CWMasterTeacherDomain
{
    //[TestFixture]
    //class ClassMeetingDomainObjTest
    //{
    //    private ClassMeetingDomainObjBasic basicObj;
    //    private ClassMeetingDomainObj domainObj;

    //    [SetUp]
    //    protected void setup()
    //    {
    //        basicObj = new ClassMeetingDomainObjBasic();
    //        domainObj = new ClassMeetingDomainObj(basicObj);
    //        domainObj.ClassSectionId = 1;
    //        domainObj.MeetingDate = new DateTime(2016, 10, 10);
    //        domainObj.StartTime = new DateTime(2016, 10, 10, 0, 0, 0);
    //        domainObj.EndTime = new DateTime(2016, 10, 10, 0, 0, 0);
    //        domainObj.MeetingNumber = 2;
    //        domainObj.Comment = "Some comment";
    //        domainObj.IsNoClass = true;
    //        domainObj.IsExamDay = false;
    //        domainObj.IsBeginningOfWeek = false;
    //        domainObj.IsReadyToTeach = true;
    //        domainObj.ClassSectionName = "Some class section name";
    //    }

    //    [Test]
    //    public void testGetIdReturnsBasicObjId()
    //    {
    //        Assert.AreEqual(basicObj.Id, domainObj.Id);
    //    }

    //    [Test]
    //    public void testGetClassMeetingIdReturnsId()
    //    {
    //        Assert.AreEqual(domainObj.Id, domainObj.ClassMeetingId);
    //    }

    //    [Test]
    //    public void testGetClassSectionId()
    //    {
    //        Assert.AreEqual(1, domainObj.ClassSectionId);
    //    }

    //    [Test]
    //    public void testGetMeetingDate()
    //    {
    //        Assert.AreEqual(new DateTime(2016, 10, 10, 0, 0, 0), domainObj.MeetingDate);
    //    }

    //    [Test]
    //    public void testGetStartTime()
    //    {
    //        Assert.AreEqual(new DateTime(2016, 10, 10, 0, 0, 0), domainObj.StartTime);
    //    }

    //    [Test]
    //    public void testGetEndTime()
    //    {
    //        Assert.AreEqual(new DateTime(2016, 10, 10, 0, 0, 0), domainObj.EndTime);
    //    }

    //    [Test]
    //    public void testGetMeetingNumber()
    //    {
    //        Assert.AreEqual(2, domainObj.MeetingNumber);
    //    }

    //    [Test]
    //    public void testGetComment()
    //    {
    //        Assert.AreEqual("Some comment", domainObj.Comment);
    //    }

    //    [Test]
    //    public void testGetNoClass()
    //    {
    //        Assert.AreEqual(true, domainObj.IsNoClass);
    //    }

    //    [Test]
    //    public void testGetIsExamDay()
    //    {
    //        Assert.AreEqual(false, domainObj.IsExamDay);
    //    }

    //    [Test]
    //    public void testGetIsBeginningOfWeek()
    //    {
    //        Assert.AreEqual(false, domainObj.IsBeginningOfWeek);
    //    }

    //    [Test]
    //    public void testGetIsReadyToTeach()
    //    {
    //        Assert.AreEqual(true, domainObj.IsReadyToTeach);
    //    }

    //    [Test]
    //    public void testGetClassSectionName()
    //    {
    //        Assert.AreEqual("Some class section name", domainObj.ClassSectionName);
    //    }

    //    [Test]
    //    public void testGetDisplayNameNoClassIsTrueHasComment()
    //    {
    //        string expected = String.Format("{0:ddd, MMM d, yyyy}", new DateTime(2016, 10, 10))
    //            + "   (No Class: " + "Some comment" + ")";
    //        Assert.AreEqual(expected, domainObj.DisplayName);
    //    }

    //    [Test]
    //    public void testGetDisplayNameNoClassIsFalseDoesntHaveComment()
    //    {
    //        domainObj.IsNoClass = false;
    //        string expected = String.Format("{0:ddd, MMM d, yyyy}", new DateTime(2016, 10, 10));
    //        Assert.AreEqual(expected, domainObj.DisplayName);
    //    }

    //    [Test]
    //    public void testGetDisplayNameWithTimeNoClassIsTrueHasComment()
    //    {
    //        string expected = String.Format("{0:ddd, MMM d, yyyy}", new DateTime(2016, 10, 10))
    //            + "   (No Class: " + "Some comment" + ")";
    //        Assert.AreEqual(expected, domainObj.DisplayNameWithTime);
    //    }

    //    [Test]
    //    public void testGetDisplayNameWithTimeNoClassIsFalseDoesntHaveComment()
    //    {
    //        domainObj.IsNoClass = false;
    //        string expected = String.Format("{0:ddd, MMM d, yyyy}", new DateTime(2016, 10, 10))
    //            + "...."
    //            + DomainUtilities.ConvertUtcToLocalTime(new DateTime(2016, 10, 10, 0, 0, 0)).ToString("hh:mm tt", CultureInfo.InvariantCulture);
    //        Assert.AreEqual(expected, domainObj.DisplayNameWithTime);
    //    }

    //    [Test]
    //    public void testGetLongDisplayName()
    //    {
    //        string expected = "Some class section name" + "........"
    //            + String.Format("{0:ddd, MMM d, yyyy}", new DateTime(2016, 10, 10))
    //            + "   (No Class: " + "Some comment" + ")";
    //        Assert.AreEqual(expected, domainObj.LongDisplayName);
    //    }

    //    [Test]
    //    public void testGetStartTimeLocal()
    //    {
    //        var expected = DomainUtilities.ConvertUtcToLocalTime(new DateTime(2016, 10, 10, 0, 0, 0));
    //        Assert.AreEqual(expected, domainObj.StartTimeLocal);
    //    }

    //    [Test]
    //    public void testGetEndTimeLocal()
    //    {
    //        var expected = DomainUtilities.ConvertUtcToLocalTime(new DateTime(2016, 10, 10, 0, 0, 0));
    //        Assert.AreEqual(expected, domainObj.EndTimeLocal);
    //    }


    //    // Setter tests.
    //    [Test]
    //    public void testSetEndTimeLocal()
    //    {
    //        var expected = new DateTime(2016, 11, 11);
    //        domainObj.EndTimeLocal = expected;
    //        Assert.AreEqual(expected, domainObj.EndTimeLocal);
    //    }

    //    [Test]
    //    public void testSetStartTimeLocal()
    //    {
    //        var expected = new DateTime(2016, 12, 01);
    //        domainObj.StartTimeLocal = expected;
    //        Assert.AreEqual(expected, domainObj.StartTimeLocal);
    //    }

    //    [Test]
    //    public void testSetIsReadyToTeach()
    //    {
    //        var expected = false;
    //        domainObj.IsReadyToTeach = expected;
    //        Assert.AreEqual(expected, domainObj.IsReadyToTeach);
    //    }

    //    [Test]
    //    public void testSetIsBeginningOfWeek()
    //    {
    //        var expected = true;
    //        domainObj.IsBeginningOfWeek = expected;
    //        Assert.AreEqual(expected, domainObj.IsBeginningOfWeek);
    //    }

    //    [Test]
    //    public void testSetIsExamDay()
    //    {
    //        var expected = false;
    //        domainObj.IsExamDay = expected;
    //        Assert.AreEqual(expected, domainObj.IsExamDay);
    //    }

    //    [Test]
    //    public void testSetNoClass()
    //    {
    //        var expected = false;
    //        domainObj.IsNoClass = expected;
    //        Assert.AreEqual(expected, domainObj.IsNoClass);
    //    }

    //    [Test]
    //    public void testSetComment()
    //    {
    //        var expected = "a new comment";
    //        domainObj.Comment = expected;
    //        Assert.AreEqual(expected, domainObj.Comment);
    //    }

    //    [Test]
    //    public void testSetMeetingNumber()
    //    {
    //        var expected = 10;
    //        domainObj.MeetingNumber = expected;
    //        Assert.AreEqual(expected, domainObj.MeetingNumber);
    //    }

    //    [Test]
    //    public void testSetEndTime()
    //    {
    //        var expected = new DateTime(2017, 01, 31, 06, 40, 25);
    //        domainObj.EndTime = expected;
    //        Assert.AreEqual(expected, domainObj.EndTime);
    //    }
        
    //    [Test]
    //    public void testSetStartTime()
    //    {
    //        var expected = new DateTime(2018, 11, 12, 13, 14, 15);
    //        domainObj.StartTime = expected;
    //        Assert.AreEqual(expected, domainObj.StartTime);
    //    }

    //    [Test]
    //    public void testSetMeetingDate()
    //    {
    //        var expected = new DateTime(12, 12, 12, 12, 12, 12);
    //        domainObj.MeetingDate = expected;
    //        Assert.AreEqual(expected, domainObj.MeetingDate);
    //    }

    //    [Test]
    //    public void testSetClassSectionId()
    //    {
    //        var expected = 200;
    //        domainObj.ClassSectionId = expected;
    //        Assert.AreEqual(expected, domainObj.ClassSectionId);
    //    }
    //}
}
