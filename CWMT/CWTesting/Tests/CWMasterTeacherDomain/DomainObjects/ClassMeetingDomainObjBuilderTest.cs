﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using CWMasterTeacherDataModel;
using CWMasterTeacherDataModel.ObjectBuilders;
using CWMasterTeacherDomain.DomainObjects;

namespace CWTesting.Tests.CWMasterTeacherDomain
{
    [TestFixture]
    class ClassMeetingDomainObjBuilderTest
    {
        //private Mock<ClassMeetingRepo> mockRepo;
        //private ClassMeeting mockClassMeeting;
        //private ClassMeetingDomainObjBuilder builder;

        //private Mock<ClassSectionRepo> mockClassSectionRepo;
        //private ClassSection mockClassSection;


        //[SetUp]
        //public void setup()
        //{
        //    mockClassMeeting = new ClassMeeting();
        //    mockClassMeeting.ClassSectionId = 1;
        //    mockClassMeeting.ClassMeetingId = 0;
        //    mockClassMeeting.MeetingDate = new DateTime(2016, 01, 01);
        //    mockClassMeeting.StartTime = new DateTime(2016, 01, 01);
        //    mockClassMeeting.EndTime = new DateTime(2016, 01, 01);
        //    mockClassMeeting.MeetingNumber = 0;
        //    mockClassMeeting.Comment = "Some comment";
        //    mockClassMeeting.NoClass = true;
        //    mockClassMeeting.IsExamDay = true;
        //    mockClassMeeting.IsBeginningOfWeek = true;
        //    mockClassMeeting.IsReadyToTeach = true;
        //    mockClassMeeting.ClassSection = new ClassSection();
        //    mockClassMeeting.ClassSection.Name = "Some class section";
        //    mockClassMeeting.LessonUses = null;

        //    mockRepo = new Mock<ClassMeetingRepo>();
        //    mockRepo.Setup(mock => mock.GetById(It.IsAny<int>())).Returns(
        //        (int i) =>
        //        {
        //            if (i == 0) return mockClassMeeting;
        //            else return null;
        //        }
        //    );
        //    mockClassSection = new ClassSection();
        //    mockClassSection.ClassSectionId = 0;
        //    mockClassSection.CourseId = 1;
        //    mockClassSection.Name = "Some name";
        //    mockClassSection.LastDisplayedClassMeetingId = 2;


        //    mockClassSectionRepo = new Mock<ClassSectionRepo>();
        //    mockClassSectionRepo.Setup(mock => mock.GetById(It.IsAny<int>())).Returns(
        //        (int i) =>
        //        {
        //            if (i == 0) return mockClassSection;
        //            else return null;
        //        }
        //    );

        //    builder = new ClassMeetingDomainObjBuilder(mockRepo.Object, mockClassSectionRepo.Object);
        //}


        //private void assertEquality(ClassMeetingDomainObj domainObj)
        //{
        //    Assert.AreEqual(0, domainObj.Id);
        //    Assert.AreEqual(1, domainObj.ClassSectionId);
        //    Assert.AreEqual(0, domainObj.ClassMeetingId);
        //    Assert.AreEqual(new DateTime(2016, 01, 01), domainObj.MeetingDate);
        //    Assert.AreEqual(new DateTime(2016, 01, 01), domainObj.StartTime);
        //    Assert.AreEqual(new DateTime(2016, 01, 01), domainObj.EndTime);
        //    Assert.AreEqual(0, domainObj.MeetingNumber);
        //    Assert.AreEqual("Some comment", domainObj.Comment);
        //    Assert.AreEqual(true, domainObj.IsNoClass);
        //    Assert.AreEqual(true, domainObj.IsExamDay);
        //    Assert.AreEqual(true, domainObj.IsBeginningOfWeek);
        //    Assert.AreEqual(true, domainObj.IsReadyToTeach);
        //    Assert.AreEqual("Some class section", domainObj.ClassSectionName);
        //}
        
               
        //[Test]
        //public void testBuildBasic()
        //{
        //    ClassMeetingDomainObjBasic basicObj = ClassMeetingDomainObjBuilder.BuildBasic(mockClassMeeting);
        //    Assert.AreEqual(0, basicObj.Id);
        //}


        //[Test]
        //public void testBuildBasicFromId()
        //{
        //    ClassMeetingDomainObjBasic basicObj = builder.BuildBasicFromId(0);
        //    Assert.AreEqual(0, basicObj.Id);
        //}


        //[Test]
        //public void testBuild()
        //{
        //    ClassMeetingDomainObj domainObj = ClassMeetingDomainObjBuilder.Build(mockClassMeeting);
        //    Assert.NotNull(domainObj);
        //    assertEquality(domainObj);

        //}

        //[Test]
        //public void testBuildFromId()
        //{
        //    ClassMeetingDomainObj domainObj = builder.BuildFromId(0);
        //    Assert.NotNull(domainObj);
        //    assertEquality(domainObj);
        //}


        //[Test]
        //public void testBuildWithNullArgumentThrows()
        //{
        //    Assert.Throws<NullReferenceException>(delegate { ClassMeetingDomainObjBuilder.Build(null); });
        //}


        //[Test]
        //public void testBuildFromIdWithBadIdThrows()
        //{
        //    Assert.Throws<NullReferenceException>(delegate { builder.BuildFromId(-1); });
        //}


        //[Test]
        //public void testBuildBasicWithNullArgumentThrows()
        //{
        //    Assert.Throws<NullReferenceException>(delegate { ClassMeetingDomainObjBuilder.BuildBasic(null); });
        //}


        //[Test]
        //public void testBuildBasicFromIdWithBadIdThrows()
        //{
        //    Assert.Throws<NullReferenceException>(delegate { builder.BuildBasicFromId(-1); });
        //}
    }
}
