﻿using NUnit.Framework;
using CWMasterTeacherDomain.DomainObjects;

namespace CWTesting.Tests.CWMasterTeacherDataModel
{
    //[TestFixture]
    //class WorkingGroupDomainObjTest
    //{
    //    private WorkingGroupDomainObj _domainObject;

    //    [Test]
    //    [Author("Kyle L Frisbie")]
    //    public void EmptyConstructor_Test()
    //    {
    //        _domainObject = new WorkingGroupDomainObj();
    //        Assert.That(null != _domainObject);
    //        Assert.That(null != _domainObject.WorkingGroupBasicObj);
    //        Assert.That(null == _domainObject.WorkingGroupBasicObj.Name);
    //        Assert.That(0 == _domainObject.WorkingGroupBasicObj.WorkingGroupId);
    //    }

    //    [Test]
    //    [Author("Kyle L Frisbie")]
    //    public void GetAndSetBasicObject_Test()
    //    {
    //        int id = 1;
    //        string name = "testObject";
    //        WorkingGroupDomainObjBasic basicObject = new WorkingGroupDomainObjBasic(id, name);
    //        _domainObject = new WorkingGroupDomainObj();
    //        _domainObject.WorkingGroupBasicObj.Name = name;
    //        _domainObject.WorkingGroupBasicObj.WorkingGroupId = id;
    //        Assert.That(null != _domainObject);
    //        Assert.That(null != _domainObject.WorkingGroupBasicObj);
    //        Assert.That(name == _domainObject.WorkingGroupBasicObj.Name);
    //        Assert.That(id == _domainObject.WorkingGroupBasicObj.WorkingGroupId);
    //    }

    //    [Test]
    //    [Author("Kyle L Frisbie")]
    //    public void BasicObjectConstructor_Test()
    //    {
    //        int id = 1;
    //        string name = "testObject";
    //        WorkingGroupDomainObjBasic basicObject = new WorkingGroupDomainObjBasic(id, name);
    //        _domainObject = new WorkingGroupDomainObj(basicObject);
    //        Assert.That(null != _domainObject);
    //        Assert.That(null != _domainObject.WorkingGroupBasicObj);
    //        Assert.That(name == _domainObject.WorkingGroupBasicObj.Name);
    //        Assert.That(id == _domainObject.WorkingGroupBasicObj.WorkingGroupId);
    //    }
    //}
}
