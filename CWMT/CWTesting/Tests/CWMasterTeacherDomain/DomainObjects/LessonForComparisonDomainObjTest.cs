﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using CWMasterTeacherDomain.DomainObjects;
using Moq;
using CWMasterTeacherDomain;
using CWMasterTeacherService.DomainObjectBuilders;
using CWMasterTeacherDataModel;
using CWMasterTeacherDataModel.ObjectBuilders;
using CWMasterTeacherDomain.ViewObjects;

namespace CWTesting.Tests.CWMasterTeacherDomain.DomainObjects
{
	class LessonForComparisonDomainObjTest
	{
		private Mock<NarrativeDomainObjBasic> mockNarrativeDomainObjBasic;
		private Mock<NarrativeDomainObj> mockNarrativeDomainObj;
		private Mock<LessonDomainObj> mockLessonDomainObj;
		private Mock<LessonPlanDomainObj> mockLessonPlanDomainObj;
		private Mock<LessonPlanDomainObjBasic> mockLessonPlanDomainObjBasic;
		private Mock<LessonDomainObjBasic> mockLessonDomainObjBasic;
		private Mock<LessonDomainObjBasic> mockEditLessonBasic;
		private Mock<StashedLessonPlanDomainObj> mockStashedLessonPlanDomainObj;
		private Mock<UserDomainObjBasic> mockUserBasicObj;
		private Mock<DocumentDomainObjBasic> mockBasicObj;
		private Mock<DocumentDomainObj> mockDocWat, mockDoc;
		private Mock<DocumentDomainObj> mockDocPdf;
		private Mock<DocumentDomainObjBasic> mockDDOBasic;
		private Mock<DocumentUseDomainObj> mockDocumentUseDomainObj;
		private Mock<DocumentUseDomainObjBasic> mockDocumentUseDomainObjBasic;
		private Mock<DocumentDomainObj> mockDocumentDomainObj;
		private Mock<DocumentUseDomainObjBasic> mockmockmock;
		private Guid guidTest = Guid.NewGuid();
		private Guid testGuid = Guid.NewGuid();
		private bool testBool = true;
		private string testString = "wordswordswords";
		public string PDFFileName = "aaa.pdf";
		private DateTime testDate = new DateTime(2018, 04, 20);
		private DateTime minDate = new DateTime(1992, 05, 06);
		private DateTime maxDate = new DateTime(2096, 03, 04);
		[OneTimeSetUp]
		public void setUp()
		{
			mockUserBasicObj = new Mock<UserDomainObjBasic>();
			mockUserBasicObj.SetupAllProperties();
			mockUserBasicObj.Object.DisplayName = testString;
			mockBasicObj = new Mock<DocumentDomainObjBasic>();
			mockBasicObj.SetupAllProperties();
			mockBasicObj.Object.Name = testString;
			mockBasicObj.Object.Id = testGuid;

			mockDoc = new Mock<DocumentDomainObj>();
			mockDoc.SetupAllProperties();
			mockDoc.Object.BasicObj = mockBasicObj.Object;
			mockDoc.Object.UserWhoModified = mockUserBasicObj.Object;
			mockDoc.Object.DateModified = testDate;
			mockDoc.Object.ModificationNotes = testString;

			mockDocPdf = new Mock<DocumentDomainObj>();
			mockDocPdf.SetupAllProperties();
			mockDocPdf.Object.FileName = PDFFileName;

			mockDDOBasic = new Mock<DocumentDomainObjBasic>();
			mockDDOBasic.SetupAllProperties();
			mockDDOBasic.Object.Id = testGuid;
			mockDDOBasic.Object.Name = testString;


			mockNarrativeDomainObjBasic = new Mock<NarrativeDomainObjBasic>();
			mockNarrativeDomainObjBasic.SetupAllProperties();
			mockNarrativeDomainObjBasic.Object.Id = testGuid;

			mockNarrativeDomainObj = new Mock<NarrativeDomainObj>(mockNarrativeDomainObjBasic.Object);
			mockNarrativeDomainObj.SetupAllProperties();

			mockEditLessonBasic = new Mock<LessonDomainObjBasic>();
			mockEditLessonBasic.SetupAllProperties();
			mockLessonDomainObjBasic = new Mock<LessonDomainObjBasic>();
			mockLessonDomainObjBasic.SetupAllProperties();
			mockLessonDomainObj = new Mock<LessonDomainObj>(mockLessonDomainObjBasic.Object);
			mockLessonDomainObj.SetupAllProperties();
			mockLessonPlanDomainObjBasic = new Mock<LessonPlanDomainObjBasic>(testGuid, testString);
			mockLessonPlanDomainObjBasic.SetupAllProperties();
			mockStashedLessonPlanDomainObj = new Mock<StashedLessonPlanDomainObj>();
			mockStashedLessonPlanDomainObj.SetupAllProperties();
			mockDocumentDomainObj = new Mock<DocumentDomainObj>();
			mockDocumentDomainObj.SetupAllProperties();
			mockDocumentUseDomainObj = new Mock<DocumentUseDomainObj>();
			mockDocumentUseDomainObj.SetupAllProperties();
			mockDocumentUseDomainObjBasic = new Mock<DocumentUseDomainObjBasic>(testGuid);
			mockDocumentUseDomainObjBasic.SetupAllProperties();
			mockLessonPlanDomainObj = new Mock<LessonPlanDomainObj>(mockLessonPlanDomainObjBasic.Object, testString, testDate, testGuid, testBool, testString);
			mockLessonPlanDomainObj.SetupAllProperties();
			mockmockmock = new Mock<DocumentUseDomainObjBasic>(guidTest);
			mockmockmock.SetupAllProperties();
		}
		[Test]
		public void NarrativeTextTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.NarrativeObj);
			obj.CustomNarrativeText = "NotWords";
			Assert.AreEqual("NotWords", obj.NarrativeText);
			obj.NarrativeObj = mockNarrativeDomainObj.Object;
			obj.NarrativeObj.Text = testString;
			Assert.AreEqual(testString, obj.NarrativeText);

		}
		[Test]
		public void HistoryLessonPlansTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			StashedLessonPlanDomainObj sObj = new StashedLessonPlanDomainObj();
			StashedLessonPlanDomainObj sObj2 = new StashedLessonPlanDomainObj();
			StashedLessonPlanDomainObj sObj3 = new StashedLessonPlanDomainObj();
			Assert.AreEqual(new List<StashedLessonPlanDomainObj>(), obj.HistoryLessonPlans);
			List<StashedLessonPlanDomainObj> objList = new List<StashedLessonPlanDomainObj>(3);
			sObj.IsSaved = true;
			sObj.DateModified = minDate;
			sObj2.IsSaved = true;
			sObj2.DateModified = maxDate;
			sObj3.IsSaved = false;
			sObj3.DateModified = testDate;
			objList.Add(sObj);
			objList.Add(sObj2);
			objList.Add(sObj3);
			obj.StashedLessonPlans = objList;
			Assert.That(obj.HistoryLessonPlans.Contains(sObj3));


		}
		[Test]
		public void SavedLessonPlansTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			StashedLessonPlanDomainObj sObj = new StashedLessonPlanDomainObj();
			StashedLessonPlanDomainObj sObj2 = new StashedLessonPlanDomainObj();
			StashedLessonPlanDomainObj sObj3 = new StashedLessonPlanDomainObj();
			Assert.AreEqual(new List<StashedLessonPlanDomainObj>(), obj.SavedLessonPlans);
			List<StashedLessonPlanDomainObj> objList = new List<StashedLessonPlanDomainObj>(3);
			sObj.IsSaved = true;
			sObj.DateModified = minDate;
			sObj2.IsSaved = true;
			sObj2.DateModified = maxDate;
			sObj3.IsSaved = false;
			sObj3.DateModified = testDate;
			objList.Add(sObj);
			objList.Add(sObj2);
			objList.Add(sObj3);
			obj.StashedLessonPlans = objList;
			Assert.That(obj.SavedLessonPlans.Contains(sObj));


		}

		[Test]
		public void HistoryNarrativesTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			StashedNarrativeDomainObj sObj = new StashedNarrativeDomainObj();
			StashedNarrativeDomainObj sObj2 = new StashedNarrativeDomainObj();
			StashedNarrativeDomainObj sObj3 = new StashedNarrativeDomainObj();
			Assert.AreEqual(new List<StashedLessonPlanDomainObj>(), obj.HistoryNarratives);
			List<StashedNarrativeDomainObj> objList = new List<StashedNarrativeDomainObj>(3);
			sObj.IsSaved = true;
			sObj.DateModified = minDate;
			sObj2.IsSaved = true;
			sObj2.DateModified = maxDate;
			sObj3.IsSaved = false;
			sObj3.DateModified = testDate;
			objList.Add(sObj);
			objList.Add(sObj2);
			objList.Add(sObj3);
			obj.StashedNarratives = objList;
			Assert.That(obj.HistoryNarratives.Contains(sObj3));
		}
		[Test]
		public void SavedNarrativesTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			StashedNarrativeDomainObj sObj = new StashedNarrativeDomainObj();
			StashedNarrativeDomainObj sObj2 = new StashedNarrativeDomainObj();
			StashedNarrativeDomainObj sObj3 = new StashedNarrativeDomainObj();
			Assert.AreEqual(new List<StashedLessonPlanDomainObj>(), obj.SavedNarratives);
			List<StashedNarrativeDomainObj> objList = new List<StashedNarrativeDomainObj>(3);
			sObj.IsSaved = true;
			sObj.DateModified = minDate;
			sObj2.IsSaved = true;
			sObj2.DateModified = maxDate;
			sObj3.IsSaved = false;
			sObj3.DateModified = testDate;
			objList.Add(sObj);
			objList.Add(sObj2);
			objList.Add(sObj3);
			obj.StashedNarratives = objList;
			Assert.That(obj.SavedNarratives.Contains(sObj));

		}

		[Test]
		public void AllDocumentUsesTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.AreEqual(new List<DocumentDomainObj>(), obj.AllDocumentUses);
			obj.TeachingDocumentUses = new List<DocumentUseDomainObj>(10);
			obj.InstructorOnlyDocumentUses = new List<DocumentUseDomainObj>(9);
			obj.ReferenceDocumentUses = new List<DocumentUseDomainObj>(8);
			Assert.AreEqual(obj.TeachingDocumentUses.Concat(obj.InstructorOnlyDocumentUses).Concat(obj.ReferenceDocumentUses).ToList(),
							obj.AllDocumentUses);
		}

		[Test]
		public void SetAllSelectedDocumentUseIdsTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.SetAllSelectedDocumentUseIds(testGuid);

			obj.TeachingDocumentUses = new List<DocumentUseDomainObj>(10);
			obj.InstructorOnlyDocumentUses = new List<DocumentUseDomainObj>(9);
			obj.ReferenceDocumentUses = new List<DocumentUseDomainObj>(8);
			Assert.IsNotNull(obj.TeachingDocumentUses);
			obj.SetAllSelectedDocumentUseIds(testGuid);


		}


		[Test]
		public void SetSelectedDocUseIdsTest()
		{
			DocumentUseDomainObj docUseObj = new DocumentUseDomainObj(null, mockDoc.Object, testGuid, false, false, false, false, false, testString, testDate);
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);

			obj.TeachingDocumentUses = new List<DocumentUseDomainObj>(10);
			obj.TeachingDocumentUses.Add(docUseObj);

			obj.InstructorOnlyDocumentUses = new List<DocumentUseDomainObj>(9);
			obj.InstructorOnlyDocumentUses.Add(docUseObj);

			obj.ReferenceDocumentUses = new List<DocumentUseDomainObj>(8);
			obj.ReferenceDocumentUses.Add(docUseObj);
			obj.SetAllSelectedDocumentUseIds(testGuid);
			foreach (var doc in obj.TeachingDocumentUses)
			{
				Assert.AreEqual(doc.SelectedDocumentUseId, testGuid);
			}
			foreach (var doc in obj.InstructorOnlyDocumentUses)
			{
				Assert.AreEqual(doc.SelectedDocumentUseId, testGuid);
			}
			foreach (var doc in obj.ReferenceDocumentUses)
			{
				Assert.AreEqual(doc.SelectedDocumentUseId, testGuid);
			}

		}

		[Test]
		public void SetDocumentComparisonBooleansTest_IfStatementsAreTrue()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.TeachingDocumentUses = new List<DocumentUseDomainObj>(10);
			obj.InstructorOnlyDocumentUses = new List<DocumentUseDomainObj>(9);
			obj.ReferenceDocumentUses = new List<DocumentUseDomainObj>(8);
			DocumentUseDomainObj docUseObj = new DocumentUseDomainObj(null, mockDoc.Object, testGuid, false, false, false, false, false, testString, testDate);
			obj.AllDocumentUses.Add(docUseObj);
			obj.TeachingDocumentUses.Add(docUseObj);
			obj.SetDocumentComparisonBooleans(obj.TeachingDocumentUses, minDate);
			foreach (var doc in obj.AllDocumentUses)
			{
				foreach (var xDoc in obj.TeachingDocumentUses) ;
				{

				}
			}

		}
		[Test]
		public void SetDocumentComparisonBooleansTest_IfStateMentsAreFalse()
		{

			//I cannot get this to fail the first if statement. 
			mockmockmock = new Mock<DocumentUseDomainObjBasic>(testGuid);
			mockDocumentUseDomainObjBasic = new Mock<DocumentUseDomainObjBasic>(guidTest);
			//Assert.AreNotEqual(mockmockmock.Object.DocumentUseId, mockDocumentUseDomainObjBasic.Object.DocumentUseId);
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			mockDoc.Object.OriginalDocumentId = Guid.Empty;
			obj.TeachingDocumentUses = new List<DocumentUseDomainObj>(10);
			obj.InstructorOnlyDocumentUses = new List<DocumentUseDomainObj>(9);
			obj.ReferenceDocumentUses = new List<DocumentUseDomainObj>(8);
			DocumentUseDomainObj docUseObj = new DocumentUseDomainObj(mockDocumentUseDomainObjBasic.Object, mockDoc.Object, testGuid, false, false, false, false, false, testString, testDate);
			DocumentUseDomainObj docUse = new DocumentUseDomainObj(mockmockmock.Object, mockDoc.Object, guidTest, false, false, false, false, false, testString, testDate);
			//Assert.AreNotEqual(docUseObj.DocumentId, docUse.DocumentId);
			obj.AllDocumentUses.Add(docUseObj);
			obj.TeachingDocumentUses.Add(docUse);
			obj.SetDocumentComparisonBooleans(obj.TeachingDocumentUses, testDate);
			foreach (var doc in obj.AllDocumentUses)
			{
				foreach (var xDoc in obj.TeachingDocumentUses) ;
				{
				}
			}

		}

		[Test]
		public void GuidIdTest()
		{

			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.Id = testGuid;
			Assert.AreEqual(testGuid, obj.Id);

		}

		[Test]
		public void NameTest()
		{

			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			mockLessonDomainObjBasic = new Mock<LessonDomainObjBasic>();
			LessonDomainObjBasic cool = obj.LessonObjBasic;
			obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.Name = testString;
			Assert.AreEqual(testString, obj.Name);
		}

		[Test]
		public void IsCollapsedTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.IsCollapsed = true;
			Assert.IsTrue(obj.IsCollapsed);

		}

		[Test]
		public void IsHiddenTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.IsHidden = true;
			Assert.IsTrue(obj.IsHidden);
		}

		[Test]
		public void IsMasterTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.IsMaster = false;
			Assert.IsFalse(obj.IsMaster);
		}

		[Test]
		public void DisplayNameTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.Name = testString;
			Assert.AreEqual(testString, obj.DisplayName);

		}

		[Test]
		public void LessonPlanTextTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.LessonPlanObj);
			Assert.AreEqual("_", obj.LessonPlanText);
			obj.LessonPlanObj = mockLessonPlanDomainObj.Object;
			obj.LessonPlanObj.Text = testString;
			Assert.AreEqual(testString, obj.LessonPlanText);
			obj.LessonPlanText = testString;
			Assert.AreEqual(testString, obj.LessonPlanText);
		}

		[Test]
		public void OldestDateModifiedTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonPlanObj = mockLessonPlanDomainObj.Object;
			obj.NarrativeObj = mockNarrativeDomainObj.Object;
			obj.LessonPlanObj.DateModified = maxDate;
			obj.NarrativeObj.DateModified = testDate;
			obj.DocumentsModifiedDate = minDate;
			Assert.AreEqual(minDate, obj.OldestDateModified);
		}

		[Test]
		public void ShortExtendedDisplayNameTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);

			obj.LessonObjBasic.Id = testGuid;
			obj.LessonObjBasic.IsMaster = false;
			obj.LessonObjBasic.IsHidden = false;
			obj.LessonObjBasic.Name = "Beaty";
			obj.UserName = "AwesomePossum180";
			Assert.AreEqual("AwesomePossum180's Beaty", obj.ShortExtendedDisplayName);
			obj.LessonObjBasic.IsMaster = true;
			obj.LessonObjBasic.IsCollapsed = true;
			obj.LessonObjBasic.IsHidden = true;
			Assert.AreEqual("Master Beaty >>Collapsed<<_(Hidden)", obj.ShortExtendedDisplayName);
			obj.LessonObjBasic.Id = Guid.Empty;
			Assert.AreEqual("_", obj.ShortExtendedDisplayName);
		}

		[Test]
		public void IsEmptyTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);

			obj.LessonObjBasic.Id = Guid.Empty;
			Assert.IsTrue(obj.IsEmpty);
		}


		[Test]
		public void LessonPlanIdTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.LessonPlanObj);
			Assert.AreEqual(null, obj.LessonPlanObj);
			obj.LessonPlanObj = mockLessonPlanDomainObj.Object;
			//obj.LessonPlanObj.Id = testGuid;
			Assert.AreEqual(testGuid, obj.LessonPlanId);

		}

		[Test]
		public void LessonPlanModifiedDate()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.LessonPlanObj);
			Assert.AreEqual(new DateTime(), obj.LessonPlanModifiedDate);
			obj.LessonPlanObj = mockLessonPlanDomainObj.Object;
			obj.LessonPlanObj.DateModified = testDate;
			Assert.AreEqual(testDate, obj.LessonPlanModifiedDate);
		}

		[Test]
		public void LessonPlanCreateDate()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.LessonPlanObj);
			Assert.AreEqual(new DateTime(), obj.LessonPlanCreatedDate);
			obj.LessonPlanObj = mockLessonPlanDomainObj.Object;
			obj.LessonPlanObj.DateCreated = testDate;
			Assert.AreEqual(testDate, obj.LessonPlanCreatedDate);
		}

		[Test]
		public void LessonPlanReferenceDateChoiceConfirmedTest()
		{
			LessonDomainObjBasic temp = mockLessonDomainObjBasic.Object;
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(null);
			Assert.IsNull(obj.LessonObjBasic);
			Assert.AreEqual(new DateTime(), obj.LessonPlanReferenceDateChoiceConfirmed);
			mockLessonDomainObjBasic = new Mock<LessonDomainObjBasic>();
			LessonDomainObjBasic cool = obj.LessonObjBasic;
			obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.LessonPlanReferenceDateChoiceConfirmed = testDate;
			Assert.AreEqual(testDate, obj.LessonPlanReferenceDateChoiceConfirmed);
		}

		[Test]
		public void LessonPlanDateChoiceConfirmedTest()
		{

			LessonDomainObjBasic temp = mockLessonDomainObjBasic.Object;
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(null);
			Assert.IsNull(obj.LessonObjBasic);
			Assert.AreEqual(new DateTime(), obj.LessonPlanDateChoiceConfirmed);
			mockLessonDomainObjBasic = new Mock<LessonDomainObjBasic>();
			LessonDomainObjBasic cool = obj.LessonObjBasic;
			obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);

			obj.LessonObjBasic.LessonPlanDateChoiceConfirmed = testDate;
			Assert.AreEqual(testDate, obj.LessonPlanDateChoiceConfirmed);
		}

		[Test]
		public void LessonPlanHasNewChangesTest()
		{
			mockLessonDomainObjBasic.Object.LessonPlanDateChoiceConfirmed = testDate;
			LessonDomainObjBasic temp = mockLessonDomainObjBasic.Object;
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(null);
			Assert.IsNull(obj.LessonObjBasic);
			Assert.IsFalse(obj.LessonPlanHasNewChanges);
			mockLessonDomainObjBasic = new Mock<LessonDomainObjBasic>();
			LessonDomainObjBasic cool = obj.LessonObjBasic;
			obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.LessonPlanGroupMostRecentModDate = testDate; //returns true if this date is greater
			obj.LessonObjBasic.LessonPlanDateChoiceConfirmed = minDate; // than this date
			Assert.IsTrue(obj.LessonPlanHasNewChanges);


		}

		[Test]
		public void NarrativeModifiedDateTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.NarrativeObj);
			Assert.AreEqual(new DateTime(), obj.NarrativeModifiedDate);
			obj.NarrativeObj = mockNarrativeDomainObj.Object;
			obj.NarrativeObj.DateModified = testDate;
			Assert.AreEqual(testDate, obj.NarrativeModifiedDate);
		}

		[Test]
		public void NarativeTextLengthTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.NarrativeObj);
			Assert.AreEqual(0, obj.NarrativeTextLength);
			obj.NarrativeObj = mockNarrativeDomainObj.Object;
			obj.NarrativeObj.Text = "Stuff and that";
			Assert.AreEqual(("Stuff and that").Length, obj.NarrativeTextLength);
		}

		[Test]
		public void NarrativeReferenceDateChoiceConfirmedTest()
		{
			mockLessonDomainObjBasic = new Mock<LessonDomainObjBasic>();

			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(null);
			Assert.IsNull(obj.LessonObjBasic);
			Assert.AreEqual(new DateTime(), obj.NarrativeReferenceDateChoiceConfirmed);

			LessonDomainObjBasic cool = obj.LessonObjBasic;
			obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.NarrativeReferenceDateChoiceConfirmed = testDate;
			Assert.AreEqual(testDate, obj.NarrativeReferenceDateChoiceConfirmed);

		}

		[Test]
		public void NarrativeDateChoiceConfirmedTest()
		{

			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(null);
			Assert.IsNull(obj.LessonObjBasic);
			Assert.AreEqual(new DateTime(), obj.NarrativeDateChoiceConfirmed);

			obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.NarrativeDateChoiceConfirmed = testDate;
			Assert.AreEqual(testDate, obj.NarrativeDateChoiceConfirmed);

		}

		[Test]
		public void NarrativeHasNewChangesTest()
		{

			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(null);
			Assert.IsNull(obj.LessonObjBasic);
			Assert.IsFalse(obj.NarrativeHasNewChanges);

			obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.NarrativeGroupMostRecentModDate = maxDate;
			obj.LessonObjBasic.NarrativeDateChoiceConfirmed = minDate;
			Assert.IsTrue(obj.NarrativeHasNewChanges);


		}
		[Test]
		public void DateDocumentsModifiedTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(null);
			Assert.IsNull(obj.LessonObjBasic);
			Assert.AreEqual(new DateTime(), obj.DateDocumentsModified);
			obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.DocumentsDateModified = testDate;
			Assert.IsNotNull(obj.LessonObjBasic);
			Assert.AreEqual(testDate, obj.DateDocumentsModified);

		}

		[Test]
		public void DocumentsDateChoiceConfirmedTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(null);
			Assert.IsNull(obj.LessonObjBasic);
			Assert.AreEqual(new DateTime(), obj.DocumentsDateChoiceConfirmed);
			LessonDomainObjBasic cool = obj.LessonObjBasic;
			obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.DocumentsDateChoiceConfirmed = testDate;
			Assert.IsNotNull(obj.LessonObjBasic);
			Assert.AreEqual(testDate, obj.DocumentsDateChoiceConfirmed);

		}
		[Test]
		public void HasComparisonDocumentsAvailableTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(null);
			Assert.IsNull(obj.LessonObjBasic);
			Assert.IsFalse(obj.HasComparisonDocumentsAvailable);
			LessonDomainObjBasic cool = obj.LessonObjBasic;
			obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.DocumentsGroupMostRecentModDate = maxDate;
			obj.LessonObjBasic.DocumentsDateChoiceConfirmed = minDate;
			Assert.IsTrue(obj.HasComparisonDocumentsAvailable);
		}

		[Test]
		public void NarrativeEditableDateConfirmedTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.EditableLessonObj);
			Assert.AreEqual(new DateTime(), obj.NarrativeEditableDateConfirmed);
			obj.EditableLessonObj = mockLessonDomainObjBasic.Object;
			obj.EditableLessonObj.NarrativeDateChoiceConfirmed = testDate;
			Assert.AreEqual(testDate, obj.NarrativeEditableDateConfirmed);

		}
		[Test]
		public void NarrativeHasNewChangesRelToGivenLessonTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			LessonDomainObjBasic cool = obj.LessonObjBasic;
			obj.NarrativeObj = mockNarrativeDomainObj.Object;
			obj.EditableLessonObj = mockEditLessonBasic.Object;
			obj.NarrativeObj.DateModified = maxDate;
			obj.EditableLessonObj.NarrativeDateChoiceConfirmed = minDate;
			Assert.Greater(maxDate, minDate);
			obj.LessonObjBasic.Id = testGuid;     //Cant Get the ID and EditableLessonObjId to be different
			obj.EditableLessonObj.Id = guidTest;
			Assert.AreNotEqual(obj.Id, obj.EditableLessonObj.Id);
			obj.NarrativeObj.Text = "LongerThanThree";
			Assert.Greater(("LongerThanThree").Length, 3);
			Assert.IsTrue(obj.NarrativeHasNewChangesRelToGivenLesson);


		}
		[Test]
		public void LessonPlanEditableDateConfirmedTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.EditableLessonObj);
			Assert.AreEqual(new DateTime(), obj.LessonPlanEditableDateConfirmed);
			obj.EditableLessonObj = mockLessonDomainObjBasic.Object;
			obj.EditableLessonObj.LessonPlanDateChoiceConfirmed = testDate;
			Assert.AreEqual(testDate, obj.LessonPlanEditableDateConfirmed);

		}
		[Test]
		public void LessonPlanHasNewChangesRelToGivenLessonTest()
		{

			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonPlanObj = mockLessonPlanDomainObj.Object;
			obj.LessonPlanObj.DateModified = maxDate;
			obj.EditableLessonObj = mockEditLessonBasic.Object;
			obj.EditableLessonObj.LessonPlanDateChoiceConfirmed = minDate; //Do i need to test for false?
			obj.EditableLessonObj.Id = testGuid;
			obj.LessonObjBasic.Id = guidTest;
			Assert.IsTrue(obj.LessonPlanHasNewChangesRelToGivenLesson);

		}

		[Test]
		public void DocumentsEditableDateConfirmedTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.EditableLessonObj);
			Assert.AreEqual(new DateTime(), obj.DocumentsEditableDateConfirmed);
			obj.EditableLessonObj = mockLessonDomainObjBasic.Object;
			obj.EditableLessonObj.DocumentsDateChoiceConfirmed = testDate;
			Assert.AreEqual(testDate, obj.DocumentsEditableDateConfirmed);

		}
		[Test]
		public void DocumentsHaveNewChangesRelToGivenLessonTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.DocumentsModifiedDate = maxDate;
			obj.EditableLessonObj = mockEditLessonBasic.Object;
			obj.EditableLessonObj.Id = testGuid;
			obj.EditableLessonObj.DocumentReferenceDateChoiceConfirmed = minDate;
			Assert.IsTrue(obj.DocumentsHaveNewChangesRelToGivenLesson);


		}
		[Test]
		public void LessonHasNewChangesRelToGivenLessonTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.DocumentsModifiedDate = maxDate;
			obj.EditableLessonObj = mockEditLessonBasic.Object;
			obj.EditableLessonObj.Id = testGuid;
			obj.EditableLessonObj.DocumentReferenceDateChoiceConfirmed = minDate;
			obj.NarrativeObj = mockNarrativeDomainObj.Object;
			obj.NarrativeObj.DateModified = minDate;
			obj.EditableLessonObj.NarrativeDateChoiceConfirmed = maxDate;
			Assert.AreEqual(true, obj.LessonHasNewChangesRelToGivenLesson);


		}
		[Test]
		public void LessonHasNewChangesRelToGivenLesson_NarrativeIsTrue()
		{

			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.NarrativeObj = mockNarrativeDomainObj.Object;
			obj.EditableLessonObj = mockEditLessonBasic.Object;
			obj.EditableLessonObj.Id = Guid.Empty;
			obj.NarrativeObj.DateModified = minDate;
			obj.EditableLessonObj.NarrativeDateChoiceConfirmed = maxDate;
			obj.DocumentsModifiedDate = minDate;
			obj.EditableLessonObj.DocumentReferenceDateChoiceConfirmed = maxDate;
			Assert.AreEqual(false, obj.NarrativeHasNewChangesRelToGivenLesson);
			Assert.AreEqual(false, obj.DocumentsHaveNewChangesRelToGivenLesson);
			Assert.AreEqual(false, obj.LessonHasNewChangesRelToGivenLesson);
		}

		[Test]
		public void EditableLessonIdTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.EditableLessonObj);
			Assert.AreEqual(Guid.Empty, obj.EditableLessonId);
			obj.EditableLessonObj = mockLessonDomainObjBasic.Object;
			obj.EditableLessonObj.Id = testGuid;
			Assert.AreEqual(testGuid, obj.EditableLessonId);

		}

		[Test]
		public void EditableNarrativeId()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.EditableLessonObj);
			Assert.AreEqual(Guid.Empty, obj.EditableNarrativeId);
			obj.EditableLessonObj = mockLessonDomainObjBasic.Object;
			obj.EditableLessonObj.NarrativeId = testGuid;
			Assert.AreEqual(testGuid, obj.EditableNarrativeId);

		}

		[Test]
		public void EditableLessonPlanIdTest()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			Assert.IsNull(obj.EditableLessonObj);
			Assert.AreEqual(Guid.Empty, obj.LessonPlanId);
			obj.EditableLessonObj = mockLessonDomainObjBasic.Object;
			obj.EditableLessonObj.LessonPlanId = testGuid;
			Assert.AreEqual(testGuid, obj.EditableLessonPlanId);
		}

		[Test]
		public void LessonIdString()
		{
			LessonForComparisonDomainObj obj = new LessonForComparisonDomainObj(mockLessonDomainObjBasic.Object);
			obj.LessonObjBasic.Id = testGuid;
			Assert.AreEqual(obj.Id.ToString(), obj.LessonIdString);
		}

	}
}