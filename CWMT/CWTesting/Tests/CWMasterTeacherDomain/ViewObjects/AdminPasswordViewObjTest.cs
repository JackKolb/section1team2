using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using CWMasterTeacherDomain.DomainObjects;
using CWMasterTeacherDomain.ViewObjects;

namespace CWTesting.Tests.CWMasterTeacherDomain.ViewObjects
{
    [Author("Abinet Kenore, Spring 2018")]

    public class AdminResetPasswordViewObjTest
    {
        private AdminResetPasswordViewObj AdminResetviewPwObj;
        private Mock<UserDomainObj> mockUserDomainObj;
        private Mock<StudentUserDomainObj> mockStudentUserDomainObj;
        private Mock<StudentUserDomainObjBasic> mockStudentUserDomainObjBasic;
        private Mock<UserDomainObjBasic> mockUserDomainObjBasic;
        private Guid testGuid = Guid.NewGuid();
        private Guid testGuid1 = Guid.NewGuid();

        [SetUp]
        public void Setup()
        {
            mockUserDomainObj = new Mock<UserDomainObj>();
            mockStudentUserDomainObj = new Mock<StudentUserDomainObj>();
            mockStudentUserDomainObjBasic = new Mock<StudentUserDomainObjBasic>();
            mockUserDomainObjBasic = new Mock<UserDomainObjBasic>();

            mockUserDomainObj.SetupAllProperties();
            mockStudentUserDomainObj.SetupAllProperties();
            mockUserDomainObjBasic.SetupAllProperties();
            AdminResetviewPwObj = new AdminResetPasswordViewObj();

        }
        [Test]
        public void SelectedInstructorUserIdTest()
        {
            AdminResetPasswordViewObj obj = new AdminResetPasswordViewObj();
            Assert.IsNull(obj.SelectedInstructorUserObj);
            Assert.AreEqual(null, obj.SelectedInstructorUserObj);
            //MockInstruct obj 
            obj.SelectedInstructorUserObj = mockUserDomainObj.Object;
            obj.SelectedInstructorUserObj.Id = testGuid;
            Assert.AreEqual(testGuid, obj.SelectedInstructorUserObj.Id);
        }

        [Test]
        public void SelectedStudentUserIdTest()
        {
            AdminResetPasswordViewObj obj = new AdminResetPasswordViewObj();
            Assert.IsNull(obj.SelectedStudentUserObj);
            Assert.AreEqual(null, obj.SelectedStudentUserObj);

            mockStudentUserDomainObjBasic.Object.Id = testGuid1;
            mockStudentUserDomainObj.Object.StudentUserBasic = mockStudentUserDomainObjBasic.Object;
            obj.SelectedStudentUserObj = mockStudentUserDomainObj.Object;
            Assert.AreEqual(testGuid1, obj.SelectedStudentUserId);

        }

        [Test]
        public void UserNameTest()
        {
            AdminResetPasswordViewObj obj = new AdminResetPasswordViewObj();

            obj.IsInstructorNotStudent = true;
            Assert.IsNull(obj.SelectedInstructorUserObj);
            Assert.AreEqual("", obj.UserName);
            obj.SelectedInstructorUserObj = mockUserDomainObj.Object;
            obj.SelectedInstructorUserObj.UserName = "test";
            Assert.AreEqual("test", obj.UserName);
            obj.IsInstructorNotStudent = false;
            Assert.IsNull(obj.SelectedStudentUserObj);
            Assert.AreEqual("", obj.UserName);
            obj.SelectedStudentUserObj = mockStudentUserDomainObj.Object;
            obj.SelectedStudentUserObj.UserName = "test";
            Assert.AreEqual("test", obj.UserName);

        }

        [Test]
        public void UserIsWorkingGroupAdminTest()
        {
            AdminResetPasswordViewObj obj = new AdminResetPasswordViewObj();
            Assert.IsNull(obj.CurrentUserObj);
            Assert.IsFalse(obj.UserIsWorkingGroupAdmin);
            obj.CurrentUserObj = mockUserDomainObj.Object;
            obj.CurrentUserObj.IsWorkingGroupAdmin = true;
            Assert.AreEqual(true, obj.UserIsWorkingGroupAdmin);
        }

        [Test]
        public void HeadingTest()
        {
            AdminResetPasswordViewObj obj = new AdminResetPasswordViewObj();
            obj.IsInstructorNotStudent = true;
            Assert.AreEqual("Reset Instructor Password", obj.Heading);
            obj.IsInstructorNotStudent = false;
            Assert.AreEqual("Reset Student Password", obj.Heading);
        }
        //March 09, 18

        [Test]
        public void UserIsEditorTest()
        {
            AdminResetPasswordViewObj obj = new AdminResetPasswordViewObj();
            obj.CurrentUserObj = mockUserDomainObj.Object;
            obj.CurrentUserObj.IsMasterEditor = true;
            Assert.AreEqual(true, obj.UserIsEditor);
        }

        [Test]
        public void UserIsApplicationAdminTest()
        {
            AdminResetPasswordViewObj obj = new AdminResetPasswordViewObj();
            obj.CurrentUserObj = mockUserDomainObj.Object;
            obj.CurrentUserObj.IsApplicationAdmin = true;
            Assert.AreEqual(true, obj.UserIsApplicationAdmin);
        }
    }
}

