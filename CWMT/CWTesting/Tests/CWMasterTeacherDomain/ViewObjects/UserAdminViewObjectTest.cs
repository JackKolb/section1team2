using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using CWMasterTeacherDomain.DomainObjects;
using CWMasterTeacherDomain.ViewObjects;

namespace CWTesting.Tests.CWMasterTeacherDomain.ViewObjects
{
    [Author("Abinet Kenore, Spring 2018")]
    public class UserAdminViewObjTest
    {

        private UserAdminViewObj userAdminViewObj;
        private Mock<UserDomainObj> mockUserDomainObj;
        private Mock<StudentUserDomainObj> mockStudentUserDomainObj;
        private Mock<StudentUserDomainObjBasic> mockStudentUserDomainObjBasic;
        private Mock<UserDomainObjBasic> mockUserDomainObjBasic;
        private Guid TestGuid = Guid.NewGuid();

        [SetUp]
        public void Setup()
        {
            mockUserDomainObj = new Mock<UserDomainObj>();
            mockStudentUserDomainObj = new Mock<StudentUserDomainObj>();
            mockStudentUserDomainObjBasic = new Mock<StudentUserDomainObjBasic>();
            mockUserDomainObjBasic = new Mock<UserDomainObjBasic>();

            mockUserDomainObj.SetupAllProperties();
            mockStudentUserDomainObj.SetupAllProperties();
            mockUserDomainObjBasic.SetupAllProperties();
            userAdminViewObj = new UserAdminViewObj();
        }

        [Test]
        public void SubmitButtonTextTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.Id = Guid.NewGuid();
            Assert.AreEqual("Update", obj.SubmitButtonText);

            obj.UserObj.Id = Guid.Empty;
            Assert.AreEqual("Create", obj.SubmitButtonText);

        }

        [Test]
        public void HeadingUserAdminTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.Id = Guid.NewGuid();
            Assert.AreEqual("Edit User", obj.Heading);

            obj.UserObj.Id = Guid.Empty;
            Assert.AreEqual("Create User", obj.Heading);
        }

        [Test]
        public void ActionNameTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.Id = Guid.NewGuid();
            Assert.AreEqual("EditUser", obj.ActionName);

            obj.UserObj.Id = Guid.Empty;
            Assert.AreEqual("RegisterCWUser", obj.ActionName);

        }

        [Test]
        public void ControllerNameTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.Id = Guid.NewGuid();
            Assert.AreEqual("UserAdmin", obj.ControllerName);

            obj.UserObj.Id = Guid.Empty;
            Assert.AreEqual("Account", obj.ControllerName);
        }

        [Test]
        public void SelectedUserIsEditorTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.IsMasterEditor = true;
            Assert.AreEqual(true, obj.SelectedUserIsEditor);
            obj.SelectedUserIsEditor = true;
            Assert.AreEqual(true, obj.SelectedUserIsEditor);
        }
        [Test]

        //0309
        public void SelectedUserIsApplicationAdminTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.IsApplicationAdmin = true;
            Assert.AreEqual(true, obj.SelectedUserIsApplicationAdmin);
            obj.SelectedUserIsApplicationAdmin = true;
            Assert.AreEqual(true, obj.SelectedUserIsApplicationAdmin);
        }
        [Test]
        public void SelectedUserIsActiveTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.IsActive = true;
            Assert.AreEqual(true, obj.SelectedUserIsActive);
            obj.UserObj.IsActive = true;
            Assert.AreEqual(true, obj.SelectedUserIsActive);


        }
        [Test]
        public void SelectedUserIsWorkingGroupAdminTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.IsWorkingGroupAdmin = true;
            Assert.AreEqual(true, obj.SelectedUserIsWorkingGroupAdmin);
            obj.SelectedUserIsWorkingGroupAdmin = true;
            Assert.AreEqual(true, obj.SelectedUserIsWorkingGroupAdmin);

        }

        [Test]
        public void SelectedUserHasAdminApprovalTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.HasAdminApproval = true;
            Assert.AreEqual(true, obj.SelectedUserHasAdminApproval);
            obj.SelectedUserHasAdminApproval = true;
            Assert.AreEqual(true, obj.SelectedUserHasAdminApproval);

        }
        //Blake
        [Test]
        public void EmailAddressTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            Assert.IsNull(obj.UserObj);
            Assert.AreEqual("", obj.EmailAddress);
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.EmailAddress = "Blake";
            Assert.AreEqual("Blake", obj.EmailAddress);
        }
        [Test]
        public void DisplayNameTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            Assert.IsNull(obj.UserObj);
            Assert.AreEqual("", obj.DisplayName);

            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.BasicObj.DisplayName = "Hudson";
            Assert.AreEqual("Hudson", obj.DisplayName);
        }
        [Test]
        public void LastNameTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            Assert.IsNull(obj.UserObj);
            Assert.AreEqual("", obj.LastName);
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.BasicObj.LastName = "Hudson";
            Assert.AreEqual("Hudson", obj.LastName);
        }
        [Test]
        public void FirstNameTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            Assert.IsNull(obj.UserObj);
            Assert.AreEqual("", obj.FirstName);
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.BasicObj.FirstName = "Blake";
            Assert.AreEqual("Blake", obj.FirstName);
        }
        [Test]
        public void UserIdTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            Assert.IsNull(obj.UserObj);
            Assert.AreEqual(Guid.Empty, obj.UserId);
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.Id = TestGuid;
            Assert.AreEqual(TestGuid, obj.UserId);

        }
        [Test]
        public void UserNameTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            Assert.IsNull(obj.UserObj);
            Assert.AreEqual("", obj.UserName);
            obj.UserObj = mockUserDomainObj.Object;
            obj.UserObj.BasicObj.UserName = "Hudsonbc";
            Assert.AreEqual("Hudsonbc", obj.UserName);
        }
        [Test]
        public void UserIsApplicationAdminTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            Assert.IsNull(obj.CurrentUserObj);
            Assert.IsFalse(obj.UserIsApplicationAdmin);
            obj.CurrentUserObj = mockUserDomainObj.Object;
            obj.CurrentUserObj.IsApplicationAdmin = true;
            Assert.IsTrue(obj.UserIsApplicationAdmin);

        }
        [Test]
        public void UserIsWorkingGroupAdminTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            Assert.IsNull(obj.CurrentUserObj);
            Assert.IsFalse(obj.UserIsWorkingGroupAdmin);
            obj.CurrentUserObj = mockUserDomainObj.Object;
            obj.CurrentUserObj.IsWorkingGroupAdmin = true;
            Assert.IsTrue(obj.UserIsWorkingGroupAdmin);
        }
        [Test]
        public void UserIsEditorTest()
        {
            UserAdminViewObj obj = new UserAdminViewObj();
            Assert.IsNull(obj.CurrentUserObj);
            Assert.IsFalse(obj.UserIsEditor);
            obj.CurrentUserObj = mockUserDomainObj.Object;
            obj.CurrentUserObj.IsMasterEditor = true;
            Assert.IsTrue(obj.UserIsEditor);
        }
    }
}
